1. **Installation**
    - Download Docker CE: https://docs.docker.com/install/
    - Clone this repository and change working directory into this project
    - Setup environment variables:
        ```
        cp .env.example .env
        ```
    - Start docker:
        ```
        ./dcp up
        ```